export let thucPhamController = {
  layThongTinTuFrom: () => {
    let ten = document.getElementById("ten").value;
    let gia = document.getElementById("gia").value;
    let moTa = document.getElementById("moTa").value;
    console.log(ten, gia, moTa);
    // object giống quy định trong mockAPI
    let thucPham = {
      name: ten,
      price: gia,
      describe: moTa,
    };
    return thucPham;
  },
  showThucPhamLenForm: (thucPham) => {
    document.getElementById("ten").value = thucPham.name;
    document.getElementById("gia").value = thucPham.price;
    document.getElementById("moTa").value = thucPham.describe;
  },
};
