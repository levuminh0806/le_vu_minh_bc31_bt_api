export let thucPhamService = {
  layDanhSachThucPham: () => {
    return axios({
      url: "https://62b07896196a9e9870244ad9.mockapi.io/thuc-pham",
      method: "GET",
    });
  },
  xoaThucPham: (id) => {
    return axios({
      url: `https://62b07896196a9e9870244ad9.mockapi.io/thuc-pham/${id}`,
      method: "DELETE",
    });
  },
  themThucPham: (thucPham) => {
    return axios({
      url: "https://62b07896196a9e9870244ad9.mockapi.io/thuc-pham/",
      method: "POST",
      data: thucPham,
    });
  },
  layThongTinChiTiet: (id) => {
    return axios({
      url: `https://62b07896196a9e9870244ad9.mockapi.io/thuc-pham/${id}`,
      method: "GET",
    });
  },
  capNhat: (thucPham) => {
    return axios({
      url: `https://62b07896196a9e9870244ad9.mockapi.io/thuc-pham/${thucPham.id}`,
      method: "PUT",
      data: thucPham,
    });
  },
};
