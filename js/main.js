// https://62b07896196a9e9870244ad9.mockapi.io/ (: endpoint thế bằng tên dự án nhỏ)

// sd import và export để nhúng link
import { thucPhamService } from "./service/thucPhamService.js";
import { spinnerService } from "./service/spinnerService.js";
import { thucPhamController } from "./controller/thucPhamController.js";
let foodList = [];
let idFoodEdited = null; // toạ biến để null chưa có giá trị để chứa giá trị
let renderTable = (list) => {
  let contentHTML = "";
  for (let index = 0; index < list.length; index++) {
    let thucPham = list[index];
    let contentTR = `<tr>
<td>${thucPham.id}</td>
<td>${thucPham.name}</td>
<td>${thucPham.price}</td>
<td>${thucPham.describe}</td>
<td>
<button type="button" name="" id="" class="btn btn-primary" btn-lg btn-block" onclick="thongTinChiTiet(${thucPham.id})">edit</button>
<button type="button" name="" id="" class="btn btn-danger" btn-lg btn-block" onclick="xoaThucPham(${thucPham.id})">detele</button>
</td>
</tr>`;
    contentHTML += contentTR;
    //   cộng dồ trong vòng lập
  }
  document.getElementById("tbody_food").innerHTML = contentHTML;
};

// Xoá thực phẩm
let xoaThucPham = (id) => {
  spinnerService.batLoading();

  thucPhamService
    .xoaThucPham(id)
    .then((res) => {
      // console.log("res: ", res);
      // // XOÁ THÀNH CÔNG
      renderDanhSachService();
      spinnerService.tatLoading();
    })
    .catch((err) => {
      // console.log("err: ", err);
      spinnerService.tatLoading();
    });
};
window.xoaThucPham = xoaThucPham;

// thêm món
let themThucPham = () => {
  // let ten = document.getElementById("ten").value;
  // let gia = document.getElementById("gia").value;
  // let moTa = document.getElementById("moTa").value;
  // console.log(ten, gia, moTa);
  // // object giống quy định trong mockAPI
  // let thucPham = {
  //   name: ten,
  //   price: gia,
  //   describe: moTa,
  // };

  let thucPham = thucPhamController.layThongTinTuFrom();
  spinnerService.batLoading();
  thucPhamService
    .themThucPham(thucPham)
    .then((res) => {
      // alert("thành công");
      spinnerService.tatLoading();
      renderDanhSachService();
    })
    .catch((err) => {
      // alert("thất bại");
      spinnerService.tatLoading();
    });
};
window.themThucPham = themThucPham;

// Chỉnh Sửa /đem dữ liệu trong bảng in ngược ra ngoài
let thongTinChiTiet = (id) => {
  idFoodEdited = id;
  thucPhamService
    .layThongTinChiTiet(id)
    .then((res) => {
      let thucPham = res.data;
      document.getElementById("ten").value = thucPham.name;
      document.getElementById("gia").value = thucPham.price;
      document.getElementById("moTa").value = thucPham.describe;
    })
    .catch((err) => {
      console.log("LỖI", err);
    });
};
window.thongTinChiTiet = thongTinChiTiet;
// Câp nhật
let capNhat = () => {
  let thucPham = thucPhamController.layThongTinTuFrom();
  spinnerService.batLoading();
  let newThucPham = { ...thucPham, id: idFoodEdited };
  thucPhamService
    .capNhat(newThucPham)
    .then((res) => {
      // console.log("res: ", res);
      spinnerService.tatLoading();
      thucPhamController.showThucPhamLenForm({
        name: "",
        price: "",
        describe: "",
      });
      renderDanhSachService();
    })
    .catch((err) => {
      console.log("err: ", err);
      spinnerService.tatLoading();
    });
};
window.capNhat = capNhat;
//  render in danh sách ra màng hinh
let renderDanhSachService = () => {
  spinnerService.batLoading();
  // axios({
  //   url: "https://62b07896196a9e9870244ad9.mockapi.io/thuc-pham",
  //   method: "GET",
  // })
  thucPhamService
    .layDanhSachThucPham()
    .then((res) => {
      // console.log(res.data);
      // lấy data sử lý nên .data
      foodList = res.data;
      renderTable(foodList);
      spinnerService.tatLoading();
    })
    .catch((err) => {
      console.log(err);
      spinnerService.tatLoading();
    });
};
// chạy lần đầu
renderDanhSachService();
